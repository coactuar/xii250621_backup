-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 13, 2022 at 04:51 PM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `XII250621`
--

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `id` int(11) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(100) DEFAULT NULL,
  `joining_date` varchar(100) NOT NULL,
  `login_date` date DEFAULT NULL,
  `logout_date` date DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1 = logged in',
  `eventname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `faculty_new`
--

CREATE TABLE `faculty_new` (
  `id` int(50) NOT NULL,
  `Name` varchar(200) NOT NULL,
  `EmailID` varchar(200) NOT NULL,
  `City` varchar(200) NOT NULL,
  `Hospital` varchar(200) NOT NULL,
  `Last Login On` varchar(200) NOT NULL,
  `Logout Times` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_name`, `user_email`, `user_question`, `asked_at`, `eventname`, `speaker`, `answered`) VALUES
(1, 'Dr S Balaji', 'docbalaji1@gmail.com', 'Lot of COVID patients with peripheral thrombus who had embolectomy had grey, gelatinous rubbery thrombus which was difficult to remove with embolectomy- inflammatory thrombus', '2021-06-25 20:28:16', 'Abbott', 1, 0),
(2, 'Suresh', 'drsureshkumar@kmchhospitals.com', 'Both femoral are used ', '2021-06-25 21:09:11', 'Abbott', 0, 0),
(3, 'Suresh', 'drsureshkumar@kmchhospitals.com', 'Both femoral are used ', '2021-06-25 21:09:42', 'Abbott', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL,
  `token` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`, `token`) VALUES
(1, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-06-24 12:13:32', '2021-06-24 12:13:32', '2021-06-24 12:13:39', 0, 'Abbott', 'bbe0197258df52354d2a548b5b7b5a2a'),
(2, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-06-24 13:49:51', '2021-06-24 13:49:51', '2021-06-24 13:49:56', 0, 'Abbott', '73ca136932758cbdfe00809967dcabc9'),
(3, 'Marimuthu', 'marimuthupetchi@gmail.com', 'Coimbatore', 'Sri Ramakrishna Hospital', NULL, NULL, '2021-06-25 00:01:39', '2021-06-25 00:01:39', '2021-06-25 01:31:39', 0, 'Abbott', '828ef294d419fb98b1a1616d96d20df5'),
(4, 'Rithin', 'rithinraja.rajendran@abbott.com', 'Chennai', 'AV', NULL, NULL, '2021-06-25 15:10:17', '2021-06-25 15:10:17', '2021-06-25 16:40:17', 0, 'Abbott', '590e379aacf7418e502858d9360ca728'),
(5, 'Anjali', 'anjalidevi9786@gmail.com', 'Tanjore ', 'Meenakshi hospital ', NULL, NULL, '2021-06-25 17:37:43', '2021-06-25 17:37:43', '2021-06-25 19:07:43', 0, 'Abbott', 'a8ce91ddcc72b9853b8d2650fbbdbe1b'),
(6, 'Anjali', 'anjalidevi9786@gmail.com', 'Tanjore ', 'Meenakshi hospital, Tanjore ', NULL, NULL, '2021-06-25 17:38:08', '2021-06-25 17:38:08', '2021-06-25 17:38:14', 0, 'Abbott', '7fa0662a797b4ead36a2eb172b94cb0b'),
(7, 'M Krithiga', 'Kritisri.80@yahoo.co.in', 'Coimbatore', 'GKNM Hospital', NULL, NULL, '2021-06-25 18:09:16', '2021-06-25 18:09:16', '2021-06-25 18:09:39', 0, 'Abbott', 'd5c927d539e8c1d85f0f9040b40c38e7'),
(8, 'BIVIN WILSON', 'bivwilson@gmail.com', 'COIMBATORE', 'GKNM HOSPITAL', NULL, NULL, '2021-06-25 18:14:20', '2021-06-25 18:14:20', '2021-06-25 19:44:20', 0, 'Abbott', '465a7b9d364fdc463cbd918e21fd0c68'),
(9, 'mohan mani', 'dr.mohan1978@yahoo.co.in', 'coimbatore', 'KMCH HEART INSTITUTE', NULL, NULL, '2021-06-25 18:22:47', '2021-06-25 18:22:47', '2021-06-25 18:24:09', 0, 'Abbott', '2221b718427b30489ca08b688c001efe'),
(10, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-06-25 18:23:51', '2021-06-25 18:23:51', '2021-06-25 19:12:42', 0, 'Abbott', 'fe125c1b26bc99ec7edebe80b5c5a0d3'),
(11, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-06-25 19:01:25', '2021-06-25 19:01:25', '2021-06-25 20:31:25', 0, 'Abbott', '14e812c58aed57df2aa976eee39499c5'),
(12, 'Shiyam ', 'shiyam.sundar@abbott.com', 'Chennai ', 'AV', NULL, NULL, '2021-06-25 19:02:26', '2021-06-25 19:02:26', '2021-06-25 20:32:26', 0, 'Abbott', '6f8d33313b0bc9a265a934134c25b49d'),
(13, 'Suresh', 'drsureshkumar@kmchhospitals.com', 'Coimbatore', 'Kmch ', NULL, NULL, '2021-06-25 19:02:48', '2021-06-25 19:02:48', '2021-06-25 20:32:48', 0, 'Abbott', 'e348b79123a479ea3d56e3262cbfcd56'),
(14, 'Kirubakaran ', 'kirubakarankk@gmail.com', 'Chennai', 'Apollo Hospital ', NULL, NULL, '2021-06-25 19:04:32', '2021-06-25 19:04:32', '2021-06-25 20:34:32', 0, 'Abbott', '0a08e5d72d03368520c056cb4e92e0bc'),
(15, 'Sriram', 'Srivats.vee@gmail.com', 'Chennai', 'SRM', NULL, NULL, '2021-06-25 19:05:34', '2021-06-25 19:05:34', '2021-06-25 20:35:34', 0, 'Abbott', '6bfae5fa3ed206995e3699ba1e3b3a1c'),
(16, 'Avinash', 'dravinash211@gmail.com', 'Chennai', 'KIMS', NULL, NULL, '2021-06-25 19:06:58', '2021-06-25 19:06:58', '2021-06-25 20:36:58', 0, 'Abbott', 'e04b2949310f911cd090dd7cb31150f3'),
(17, 'Jagadeesh', 'jagadeesh_mmc@yahoo.co.in', 'Palakkad', 'Ahalia hospital', NULL, NULL, '2021-06-25 19:07:29', '2021-06-25 19:07:29', '2021-06-25 20:37:29', 0, 'Abbott', 'd01c046b05b230317ac2e2d6b33245bd'),
(18, 'Shiyam ', 'shiyam.sundar@abbott.com', 'Chennai ', 'AV', NULL, NULL, '2021-06-25 19:08:34', '2021-06-25 19:08:34', '2021-06-25 20:38:34', 0, 'Abbott', '5019dcb0b01f5838792908cea2bbba0d'),
(19, 'Suresh', 'drsureshkumar@kmchhospitals.com', 'Coimbatore', 'Kmch ', NULL, NULL, '2021-06-25 19:09:42', '2021-06-25 19:09:42', '2021-06-25 20:39:42', 0, 'Abbott', 'ab3967af386772008d42be0aec724250'),
(20, 'Sundar', 'Sundarsaivimal@gmail.com', 'Chennai', 'Kauvery', NULL, NULL, '2021-06-25 19:09:49', '2021-06-25 19:09:49', '2021-06-25 20:39:49', 0, 'Abbott', '58862e0cdc0d48ee6b7920e2eb582796'),
(21, 'Kasi', 'Kasirajaboopathy@gmail.com', 'Pondicherry ', 'MGM', NULL, NULL, '2021-06-25 19:11:32', '2021-06-25 19:11:32', '2021-06-25 20:41:32', 0, 'Abbott', '2e4bed7079854d5b2bbb13d87ad68e45'),
(22, 'Ashok', 'gashokmkumar@gmail.com', 'Chennai', 'Chettinad Hospital ', NULL, NULL, '2021-06-25 19:12:40', '2021-06-25 19:12:40', '2021-06-25 20:42:40', 0, 'Abbott', 'f796339952050060506d29d7f0fadcdb'),
(23, 'Bharath', 'bharathcath89@gmail.com', 'Krishnagri', 'ARK multi speciality hospital', NULL, NULL, '2021-06-25 19:12:51', '2021-06-25 19:12:51', '2021-06-25 20:42:51', 0, 'Abbott', '06b71e4873a18f88e0766bbcebda5440'),
(24, 'Raman', 'raman24398@gmail.com', 'Pollachi', 'Arun hospital', NULL, NULL, '2021-06-25 19:13:12', '2021-06-25 19:13:12', '2021-06-25 20:43:12', 0, 'Abbott', '9d1721ffedeae0e4e106bb40d37d5416'),
(25, 'Shajahan', 'shajahan8121997@gmail.com', 'Kallakurichi', 'Sri Raju hospital', NULL, NULL, '2021-06-25 19:13:50', '2021-06-25 19:13:50', '2021-06-25 20:43:50', 0, 'Abbott', 'b5f59f0065fa11f239837b23c56775b7'),
(26, 'Karthikeyan ', 'Karthikeyandm@gmail.com', 'Chennai', 'Madha', NULL, NULL, '2021-06-25 19:14:38', '2021-06-25 19:14:38', '2021-06-25 20:44:38', 0, 'Abbott', '4c4779c6e0678b490227f29a1a52d74a'),
(27, 'Lavanya', 'lavi4321@gmail.com', 'Chennai', 'Apollo Hospital ', NULL, NULL, '2021-06-25 19:15:15', '2021-06-25 19:15:15', '2021-06-25 20:45:15', 0, 'Abbott', 'e5d030219b9d59f280e99b624dc1908c'),
(28, 'Dr THIYAGARAJAN G', 'gtrajan1986@gmail.com', 'Coimbatore ', 'Sri Ramakrishna Hospital ', NULL, NULL, '2021-06-25 19:15:20', '2021-06-25 19:15:20', '2021-06-25 20:45:20', 0, 'Abbott', '730b00b852bf3ec0361f3724de551566'),
(29, 'SATHISH KUMAR T N', 'DRSATHISH85@GMAIL.COM', 'Salem', 'Kauvery hospital', NULL, NULL, '2021-06-25 19:15:54', '2021-06-25 19:15:54', '2021-06-25 20:45:54', 0, 'Abbott', '48f0d6a551f82e71847f2e6734d584c2'),
(30, 'Premnath', 'premnathvinayagm@gmail.com', 'Pondicherry ', 'MGM', NULL, NULL, '2021-06-25 19:16:07', '2021-06-25 19:16:07', '2021-06-25 20:46:07', 0, 'Abbott', 'cce959bcef089a5c06515d85241ae4ea'),
(31, 'Dr.Girish', 'Girigirias@gmail.com', 'Chennai ', 'Chennai ', NULL, NULL, '2021-06-25 19:19:03', '2021-06-25 19:19:03', '2021-06-25 20:49:03', 0, 'Abbott', '51d4d2ea2f9c3017cb2a2aa0658dcb44'),
(32, 'Senthil', 'senthilnathantth@gmail.com', 'Chennai ', 'SRM', NULL, NULL, '2021-06-25 19:21:26', '2021-06-25 19:21:26', '2021-06-25 20:51:26', 0, 'Abbott', '62625eaf36497e56c0112718e0e3e8c6'),
(33, 'Dr Vishnu prabhu', 'drvishnumddm@yahoo.co.in', 'Salem', 'Aishwaryam ', NULL, NULL, '2021-06-25 19:22:22', '2021-06-25 19:22:22', '2021-06-25 20:52:22', 0, 'Abbott', 'c1bd03731b66b02b96b9040c82b533c7'),
(34, 'Subash', 'subashthillai@gmail.com', 'Chennai', 'Venkateshwara', NULL, NULL, '2021-06-25 19:23:29', '2021-06-25 19:23:29', '2021-06-25 20:53:29', 0, 'Abbott', 'ede7c803cfb49e733cb9810d69016156'),
(35, 'KC Rathan ', 'Kamanatchanolian.rathan@abbott.com', 'Chennai ', 'Abbott ', NULL, NULL, '2021-06-25 19:24:06', '2021-06-25 19:24:06', '2021-06-25 20:54:06', 0, 'Abbott', '18432311cf9e41e70ca42b4ad498dc22'),
(36, 'Raghav', 'drraghav32@gmail.com', 'Chennai', 'Chennai', NULL, NULL, '2021-06-25 19:27:38', '2021-06-25 19:27:38', '2021-06-25 20:57:38', 0, 'Abbott', '01629cfed9389c604c74c2bedc92d9b1'),
(37, 'Bivin Wilson', 'bivwilson@gmail.com', 'Coimbatore ', 'Gknm', NULL, NULL, '2021-06-25 19:29:11', '2021-06-25 19:29:11', '2021-06-25 20:59:11', 0, 'Abbott', 'ffd00792371017a3d61fbd22bca197ab'),
(38, 'Karthik', 'karthikkannapiran@gmail.com', 'Coimbatore', 'Kumaran medical center', NULL, NULL, '2021-06-25 19:30:45', '2021-06-25 19:30:45', '2021-06-25 21:00:45', 0, 'Abbott', '82bdd49f42fef679d20bb435c219abff'),
(39, 'Neelraj.C', 'neelrajcc03@gmail.com', 'Coimbatore ', 'Kovai Medical Centre & hospital ', NULL, NULL, '2021-06-25 19:32:16', '2021-06-25 19:32:16', '2021-06-25 21:02:16', 0, 'Abbott', '42e67bfb8e6927f4d9887c19f3fd7be1'),
(40, 'Sabari krishnan', 'sabarikrishnan30@gmail.com', 'Thanjavur', 'Meenakshi hospital ', NULL, NULL, '2021-06-25 19:34:17', '2021-06-25 19:34:17', '2021-06-25 21:04:17', 0, 'Abbott', 'f744ef1a4536e5194d2b8c828c9c954a'),
(41, 'Prithiviraj ', 'sumiprithivi97@gmail.com', 'Pondicherry ', 'Surendar', NULL, NULL, '2021-06-25 19:34:42', '2021-06-25 19:34:42', '2021-06-25 21:04:42', 0, 'Abbott', '3df8184b393d96dde985cf9bfdbad08d'),
(42, 'Jagadeesh', 'jagadeesh_mmc@yahoo.co.in', 'Palakkad', 'Ahalia hospital', NULL, NULL, '2021-06-25 19:39:44', '2021-06-25 19:39:44', '2021-06-25 21:09:44', 0, 'Abbott', 'ceb5508680c2cb6d4e812417276c06ca'),
(43, 'Arunkumar Ranganathan', 'arunkumar.r@abbott.com', 'Chennai', 'Abbott vascular', NULL, NULL, '2021-06-25 19:40:38', '2021-06-25 19:40:38', '2021-06-25 21:10:38', 0, 'Abbott', '80fa81573c3b240712540cd359b2a3be'),
(44, 'Dr S Balaji', 'docbalaji1@gmail.com', 'Coimbatore', 'Sri Ramakrishna Hospital', NULL, NULL, '2021-06-25 19:43:13', '2021-06-25 19:43:13', '2021-06-25 21:13:13', 0, 'Abbott', '42e6ee42a839b6393f7b52a93ef1a926'),
(45, 'Kk', 'karthikkannapiran@gmail.com', 'Coimbatore', 'Kumaran', NULL, NULL, '2021-06-25 19:46:09', '2021-06-25 19:46:09', '2021-06-25 21:16:09', 0, 'Abbott', 'c6446e189a0e65857d4aa47674e80bd3'),
(46, 'SATHISH KUMAR T N', 'DRSATHISH85@GMAIL.COM', 'Salem', 'Kauvery hospital', NULL, NULL, '2021-06-25 19:47:57', '2021-06-25 19:47:57', '2021-06-25 21:17:57', 0, 'Abbott', 'b0d88ab85655cad12c0e72852fb8b9ee'),
(47, 'Satheesh kumar', 'satheesh31593@gmail.com', 'Tanjore', 'Meenakshi hospital', NULL, NULL, '2021-06-25 20:04:58', '2021-06-25 20:04:58', '2021-06-25 21:34:58', 0, 'Abbott', '6835a1f5e582f40757c10165ef6a71ae'),
(48, 'Dr kk ', 'karthikkannapiran@gmail.com', 'Coimbatore', 'Kumarat', NULL, NULL, '2021-06-25 20:12:43', '2021-06-25 20:12:43', '2021-06-25 21:42:43', 0, 'Abbott', 'ea0bc921fbf67f5c2fa9776379f742a2'),
(49, 'Ramkumar', 'ramkumar.p@abbott.com', 'Madurai', 'Abbott', NULL, NULL, '2021-06-25 20:16:27', '2021-06-25 20:16:27', '2021-06-25 21:46:27', 0, 'Abbott', 'e0f898934bf6ac3628fc2438444bd6fe'),
(50, 'Libin', 'libind320@gmail.com', 'Coimbatore ', 'Muthus hospital ', NULL, NULL, '2021-06-25 20:19:28', '2021-06-25 20:19:28', '2021-06-25 21:49:28', 0, 'Abbott', '88c0e8896455c1a44f47dd77b1a1eee7'),
(51, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-06-25 20:26:53', '2021-06-25 20:26:53', '2021-06-25 21:56:53', 0, 'Abbott', '132c827cc19207fd2d11b036569ab633'),
(52, 'Shajahan', 'shajahan8121997@gmail.com', 'Kallakurichi', 'Sri Raju hospital', NULL, NULL, '2021-06-25 20:29:23', '2021-06-25 20:29:23', '2021-06-25 21:59:23', 0, 'Abbott', '858f01907dc22bde80a2158680f46b48'),
(53, 'Ramkumar Rajappan', 'ramkumar.mdumc@gmail.com', 'COIMBATORE', 'GKNMH', NULL, NULL, '2021-06-25 20:31:32', '2021-06-25 20:31:32', '2021-06-25 22:01:32', 0, 'Abbott', '00f491cb7c63a99e4d72c77cd6321226'),
(54, 'Neelraj.C', 'neelrajcc03@gmail.com', 'Coimbatore ', 'Kovai Medical Centre & hospital ', NULL, NULL, '2021-06-25 20:36:20', '2021-06-25 20:36:20', '2021-06-25 22:06:20', 0, 'Abbott', '93aa442c3fad3aab18e3784cc9ca29d2'),
(55, 'Ravindran Rajendran', 'rravi_dr@rediffmail.com', 'chennai', 'APOLLO', NULL, NULL, '2021-06-25 20:38:18', '2021-06-25 20:38:18', '2021-06-25 22:08:18', 0, 'Abbott', '0f7171f38af554c82ac56bcb5c26a439'),
(56, 'Arul Shanmugam', 'arulcardiologist@gmail.com', 'Hosur', 'Kauvery hospital', NULL, NULL, '2021-06-25 20:58:34', '2021-06-25 20:58:34', '2021-06-25 22:28:34', 0, 'Abbott', '92fa7936d2edef345ada0fc8750934c0'),
(57, 'Arul Shanmugam', 'arulcardiologist@gmail.com', 'Hosur', 'Kauvery hospital', NULL, NULL, '2021-06-25 20:59:12', '2021-06-25 20:59:12', '2021-06-25 22:29:12', 0, 'Abbott', '764efac6975fd078c370ff60fa7d21d9'),
(58, 'ARUNKUMAR ', 'arunkrishnacvt@gmail.com', 'Banglore ', 'Apollo', NULL, NULL, '2021-06-25 21:12:23', '2021-06-25 21:12:23', '2021-06-25 22:42:23', 0, 'Abbott', '2311abfe71f79ca94c00b5e22ba836f7'),
(59, 'Neelraj.C', 'neelrajcc03@gmail.com', 'Coimbatore ', 'Kovai Medical Centre & hospital ', NULL, NULL, '2021-06-25 21:21:25', '2021-06-25 21:21:25', '2021-06-25 22:51:25', 0, 'Abbott', 'a922da0dac77219475f342bfbaaaa0ce'),
(60, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-06-25 21:21:50', '2021-06-25 21:21:50', '2021-06-25 21:30:57', 0, 'Abbott', 'ed307f32b7d26c1fe032d3d4ee1a9daa');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faculty_new`
--
ALTER TABLE `faculty_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `faculty_new`
--
ALTER TABLE `faculty_new`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
