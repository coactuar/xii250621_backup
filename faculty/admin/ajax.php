<?php
require_once "../config.php";

if(isset($_POST['action']) && !empty($_POST['action'])) {
    
    $action = $_POST['action'];
    
    switch($action) {
        
        
        case 'getusers':
        
            if (isset($_POST["page"])) 
            { 
                $page  = $_POST["page"]; 
            }
            else { 
                $page=1; 
            }
            
            $start_from = ($page-1) * $limit;
            $today=date('Y/m/d H:i:s');
            $sql = "SELECT COUNT(id) as count FROM faculty where eventname='$event_name' and logout_date > '$today'";  
            $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
            $row = mysqli_fetch_assoc($rs_result);
            $loggedin = $row['count'];
        
            
            $sql = "SELECT COUNT(id) as count FROM faculty where eventname='$event_name'";  
            $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
            $row = mysqli_fetch_assoc($rs_result);
            $total_records = $row['count'];  
            $total_pages = ceil($total_records / $limit);
            ?>
            <div class="row user-info">
                <div class="col-6">
                    Total Users: <?php echo $total_records; ?>
                </div>
                <div class="col-6">
                    Currently Logged In: <div id="logged-in"><?php echo $loggedin; ?></div>
                </div>
            </div> 
            <div class="row user-details">
                <div class="col-12">
                    <table class="table table-striped">
                      <thead class="thead-inverse">
                        <tr>
                          <th>Name</th>
                          <th>Email ID</th>
                          <th>City</th>
                          <th>Hospital</th>
                          <th>Registered On</th>
                          <th>Last Login Time</th>
                          <!-- <th>Last Logout Time</th> -->
                        </tr>
                      </thead>
                      <tbody>
                      <?php		
                        $query="select * from faculty where eventname='$event_name' order by logout_status desc, login_date desc LIMIT $start_from, $limit";
                        $res = mysqli_query($link, $query) or die(mysqli_error($link));
                        $loggedin = 0;
                        while($data = mysqli_fetch_assoc($res))
                        {
                        ?>
                          <tr>
                            <td><?php echo $data['user_name']; ?></td>
                            <td><?php echo $data['user_email']; ?></td>
                            <td><?php echo $data['city']; ?></td>
                            <td><?php echo $data['hospital']; ?></td>
                            <td><?php 
                                if($data['joining_date'] != ''){
                                    $date=date_create($data['joining_date']);
                                    echo date_format($date,"M d, H:i a"); 
                                }
                                else{
                                    echo '-';
                                }
                                ?>
                            </td>
                            <td><?php 
                                if($data['login_date'] != ''){
                                    $date=date_create($data['login_date']);
                                    echo date_format($date,"M d, H:i a"); 
                                }
                                else{
                                    echo '-';
                                }
                                ?>
                            </td>
                           
                          </tr>
                      <?php			
                        }
                      ?>
                  
                    </table>  
                </div>
            </div>
            <nav>
              <ul class="pagination pagination-sm" id="pagination">
                <?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                            if($i == 1):?>
                     <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php else:?>
                    <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php endif;?>
                <?php endfor;endif;?>
              </ul>
            </nav>
            <?php
        
            
        break;
        
        case 'getquestions':
        
            if (isset($_POST["page"])) 
            { 
                $page  = $_POST["page"]; 
            }
            else { 
                $page=1; 
            }
            
            $start_from = ($page-1) * $limit;
        
            $sql = "SELECT COUNT(id) FROM tbl_questions where eventname='$event_name'";  
            $rs_result = mysqli_query($link,$sql);  
            $row = mysqli_fetch_row($rs_result);  
            $total_records = $row[0];  
            $total_pages = ceil($total_records / $limit);
            ?>
            <div class="row user-info">
                <div class="col-6">
                    Total Ques: <div id="ques_count"><?php echo $total_records; ?></div>
                </div>
                <div class="col-6>"><div id="ques_update"></div></div>
            </div> 
            <div class="row user-details">
                <div class="col-12">
                    <table class="table table-striped table-light">
                      <thead class="thead-inverse">
                        <tr>
                          <th width="200">Name</th>
                          <th>Question</th>
                          <th width="200">Asked At</th>
                          <th width="300">For Speaker?</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php		
                        $query="select * from tbl_questions where eventname='$event_name' order by speaker asc, answered asc, asked_at desc LIMIT $start_from, $limit";
                        $res = mysqli_query($link, $query) or die(mysqli_error($link));
                        while($data = mysqli_fetch_assoc($res))
                        {
                        ?>
                          <tr>
                            <td><?php echo $data['user_name']; ?></td>
                            <td><?php echo $data['user_question']; ?></td>
                            <td><?php 
                                $date=date_create($data['asked_at']);
                                echo date_format($date,"M d, H:i a"); ?>
                            </td>
                            <td>
                            <?php if ($data['answered'] == '0') { ?>
                            <a href="#" class="btnSpk btn btn-sm <?php if ($data['speaker'] == '0') { echo 'btn-danger'; } else { echo 'btn-success'; } ?>" onClick="updSpk('<?php echo $data['id']; ?>','<?php echo $data['speaker']; ?>')"><?php if ($data['speaker'] == '0') { echo 'Yes/No?'; } else { echo 'Cancel?'; } ?></a>
                            <?php 
                            
                            if ($data['speaker'] == '1') { ?>
                            <a href="#" class="btnSpk btn btn-sm btn-danger" onClick="updSpkAns('<?php echo $data['id']; ?>','<?php echo $data['answered']; ?>')">Mark Answered</a>
                            <?php } 
                            }
                            else
                            {
                             ?>
                            <a href="#" class="btnSpk btn btn-sm btn-danger" onClick="updSpkAns('<?php echo $data['id']; ?>','<?php echo $data['answered']; ?>')">Mark Unanswered</a>
                            <?php    
                            }
                            ?>
                            </td>
                            
                          </tr>
                      <?php			
                        }
                      ?>
                  
                    </table>  
                </div>
            </div>   
            <nav>
              <ul class="pagination pagination-sm" id="pagination">
                <?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                            if($i == 1):?>
                     <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php else:?>
                    <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php endif;?>
                <?php endfor;endif;?>
              </ul>
            </nav>
            <?php
        
            
        break;
        case 'logoutuser':
              $sql = "update faculty set logout_status='0' where id='".$_POST['userid']."' and eventname='$event_name'";  
              $rs_result = mysqli_query($link,$sql);  
        break;
        case 'getquesupdate':
              $sql = "SELECT COUNT(id) FROM tbl_questions where eventname='$event_name'";  
              $rs_result = mysqli_query($link,$sql);  
              $row = mysqli_fetch_row($rs_result);  
              $total_records = $row[0];  
              
              echo $total_records;
        break;
        
        case 'updateques':
              $newval = 0;
              if($_POST['val'] == 0)
              {
                  $newval = 1;
              }
              else
              {
                  $newval = 0;
              }
              $sql = "Update tbl_questions set show_speaker ='$newval' where eventname='$event_name' and id = '".$_GET['id']."'";  
              $rs_result = mysqli_query($link,$sql);  
              //$row = mysqli_fetch_row($rs_result);  
              //$total_records = $row[0];  
              
              //echo $sql;
        break;
        
        case 'updatespk':
              $newval = 0;
              if($_POST['val'] == 0)
              {
                  $newval = 1;
              }
              else
              {
                  $newval = 0;
              }
              $sql = "Update tbl_questions set speaker ='$newval', answered='0' where eventname='$event_name' and id = '".$_POST['ques']."'";  
              $rs_result = mysqli_query($link,$sql);  
              //$row = mysqli_fetch_row($rs_result);  
              //$total_records = $row[0];  
              
              //echo $sql;
        break;
         case 'updatespkans':
              $newval = 0;
              if($_POST['val'] == 0)
              {
                  $newval = 1;
              }
              else
              {
                  $newval = 0;
              }
              $sql = "Update tbl_questions set answered ='$newval' where eventname='$event_name' and id = '".$_POST['ques']."'";  
              $rs_result = mysqli_query($link,$sql);  
              //$row = mysqli_fetch_row($rs_result);  
              //$total_records = $row[0];  
              
              //echo $sql;
        break;
        case 'getspkquesupdate':
        
              $sql = "SELECT COUNT(id) FROM tbl_questions where eventname='$event_name' and speaker='1' and answered='0'";  
              $rs_result = mysqli_query($link,$sql);  
              $row = mysqli_fetch_row($rs_result);  
              $total_records = $row[0];  
              
              echo $total_records;
        break;
        
          case 'faculty':

            if (isset($_POST["page"])) 
            { 
                $page  = $_POST["page"]; 
            }
            else { 
                $page=1; 
            }
            
            $start_from = ($page-1) * $limit;
            $today=date('Y/m/d H:i:s');
            $sql = "SELECT COUNT(id) as count FROM faculty_new ";  
            $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
            $row = mysqli_fetch_assoc($rs_result);
            $loggedin = $row['count'];
        
            
            $sql = "SELECT COUNT(id) as count FROM faculty_new ";  
            $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
            $row = mysqli_fetch_assoc($rs_result);
            $total_records = $row['count'];  
            $total_pages = ceil($total_records / $limit);
            ?>
             <?php  if($total_records == 0 ){
                ?>
<marquee behavior="scroll" direction="right" scrollamount="30">
<p class="display-1 text-danger">Data under process</p> 
</marquee>
                <?php
            }else{
            ?>
            <div class="row user-info">
                <div class="col-6">
                    Total Users: <?php echo $total_records; ?>
                </div>
                <div class="col-6">
                    Currently Logged In: <div id="logged-in"><?php echo $loggedin; ?></div>
                </div>
            </div> 
          
            <div class="row user-details">
           
                <div class="col-12">
                    <table class="table table-striped">
                      <thead class="thead-inverse">
                        <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>City</th>
                        <th>Hospital</th>
                        <th>Login Times</th>
                        <th>Logout Times</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php		
                        $query="select * from faculty_new ";
                        $res = mysqli_query($link, $query) or die(mysqli_error($link));
                        $loggedin = 0;
                        while($data = mysqli_fetch_assoc($res))
                        {
                        ?>
                          <tr>
                          <td><?php echo $data['Name']; ?></td>
                          <td><?php echo $data['EmailID']; ?></td>
                          <td><?php echo $data['City']; ?></td>
                          <td><?php echo $data['Hospital']; ?></td>
                          <td><?php echo $data['Last Login On']; ?></td>
                          <td><?php echo $data['Logout Times']; ?></td>
                       
                        
                       
                          </tr>
                      <?php			
                        }
                      ?>
                  
                    </table>  
                </div>
               
            </div>
            <?php }
             ?>
            <nav>
              <ul class="pagination pagination-sm" id="pagination">
                <?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                            if($i == 1):?>
                     <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php else:?>
                    <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php endif;?>
                <?php endfor;endif;?>
              </ul>
            </nav>
            <?php
        
            break;
    }
    
}


?>